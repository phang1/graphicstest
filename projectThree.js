var canvas, gl, program;
var whatToDraw = 0;

var left = -1;
var bottom = -1;
var right = 1;
var topVal = 1;
var near = 1;
var far = 3.5

var eye = [0.0, 0.0, 2.0];
var at = [0.0, 0.0, 0.0 ];
var up = [0.0, 1.0, 0.0];

var vBuffer, nBuffer;

var totalVertices = [];
var totalNormals = [];

var vPosition, vNormal;

var viewMatrix, projectionMatrix;
var viewMatrixLoc, projectionMatrixLoc;

var lightPosition = vec4(0.0, 0.0, 0.0, 0.0 );
var lightAmbient = vec4(0.2, 0.2, 0.2, 1.0 );
var lightDiffuse = vec4( 1.0, 1.0, 1.0, 1.0 );
var lightSpecular = vec4( 1.0, 1.0, 1.0, 1.0 );

var materialAmbient = vec4( 1.0, 0.0, 1.0, 1.0 );
var materialDiffuse = vec4( 1.0, 0.8, 0.0, 1.0 );
var materialSpecular = vec4( 1.0, 0.8, 0.0, 1.0 );
var materialShininess = 100.0;

window.onload = function init() {
							// get canvas handle
    canvas = document.getElementById( "gl-canvas" );
    
							// get context
/*	var ctx = canvas.getContext("experimental-webgl", 
					{preserveDrawingBuffer: true});*/

							// WebGL Initialization
    gl = WebGLUtils.setupWebGL(canvas, {preserveDrawingBuffer: true} );
    if ( !gl ) { 
		alert( "WebGL isn't available" ); 
	}

  gl.viewport( 0, 0, canvas.width, canvas.height );
  gl.clearColor( .5, 1.0, 1.0, 1.0);

  gl.enable(gl.DEPTH_TEST);

	gl.clear( gl.COLOR_BUFFER_BIT );
						//  Load shaders and initialize attribute buffers
  var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );
						// variables through which shader receives vertex
						// and other attributes
  viewMatrixLoc = gl.getUniformLocation( program, "viewMatrix" );
  projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );
  
  var vPosition = gl.getAttribLocation(program, "vPosition");
  var vNormal = gl.getAttribLocation(program, "vNormal");

  initShapes();

  vBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, flatten(totalVertices), gl.STATIC_DRAW);

  gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(vPosition);

  nBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, nBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, flatten(totalNormals), gl.STATIC_DRAW);

  gl.vertexAttribPointer(vNormal, 4, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(vNormal);

  ambientProduct = mult(lightAmbient, materialAmbient);
  diffuseProduct = mult(lightDiffuse, materialDiffuse);
  specularProduct = mult(lightSpecular, materialSpecular);

	//two onclick elements to decide which object to be drawn
	document.getElementById("teapot").onclick = 
		function() {
			whatToDraw = 0;
		};
	document.getElementById("sphere").onclick = 
		function() {
			whatToDraw = 1;
		};

    //set the uniform variables in the vertex shader

    gl.uniform4fv( gl.getUniformLocation(program, 
       "ambientProduct"),flatten(ambientProduct) );
    gl.uniform4fv( gl.getUniformLocation(program, 
       "diffuseProduct"),flatten(diffuseProduct) );
    gl.uniform4fv( gl.getUniformLocation(program, 
       "specularProduct"),flatten(specularProduct) ); 
    gl.uniform4fv( gl.getUniformLocation(program, 
       "lightPosition"),flatten(lightPosition) );
    gl.uniform1f( gl.getUniformLocation(program, 
       "shininess"),materialShininess );

	render();
}
function render(){
	gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
  setData();

    if(whatToDraw == 0){

      gl.drawArrays(gl.TRIANGLES, 720, 6048);
    } else{

      gl.drawArrays(gl.TRIANGLES, 0, 720);
    }
	
	window.requestAnimFrame(render);
}

function setData(){

  viewMatrix = lookAt(eye, at, up);
  gl.uniformMatrix4fv(viewMatrixLoc, gl.false, flatten(viewMatrix));

  projectionMatrix = frustrum();
  
  gl.uniformMatrix4fv(projectionMatrixLoc, gl.false, flatten(projectionMatrix));
}

function frustrum(){
  var a = (right + left)/(right-left);
  var b = (topVal+bottom)/(topVal-bottom);
  var c = -(far + near)/(far-near);
  var d = -(2*far*near)/(far-near);

  var result = mat4();
  result[0][0] = (2*near)/(right-left);
  result[0][2] = a;
  result[1][1] = (2*near)/(topVal-bottom);
  result[1][2] = b;
  result[2][2] = c;
  result[2][3] = d;
  result[3][2] = -1;

  return result;
}

function initShapes(){
    //store vertices of sphere and teapot into an array
    var a = 0;
    while(sph12.faces[a] !=null){

      totalVertices.push(sph12.vertices[(sph12.faces[a]*3)], sph12.vertices[(sph12.faces[a]*3)+1], sph12.vertices[(sph12.faces[a]*3)+2], 1.0);
      totalNormals.push(sph12.normals[3*a],sph12.normals[3*a+1],sph12.normals[3*a+2], sph12.normals[3*a+3]);
      a++;
    }

    while(teapot.faces[a] != null){

      totalVertices.push(teapot.vertices[(teapot.faces[a]*3)], teapot.vertices[(teapot.faces[a]*3)+1], teapot.vertices[(teapot.faces[a]*3)+2], 1.0);
      totalNormals.push(teapot.normals[3*a], teapot.normals[3*a+1], teapot.normals[3*a+2], teapot.normals[3*a+3]);
      a++;
    }
}
function updateLeft(val) {
  document.getElementById('leftInput').value=val; 
  left=val;
  

 }
function updateRight(val) { 
  document.getElementById('rightInput').value=val;
  right=val;
  
}
function updateBottom(val) { 
  document.getElementById('bottomInput').value=val;
  bottom= val;
  
}
function updateTop(val) { 
  document.getElementById('topInput').value=val;
  topVal=val;
  
}
function updateNear(val) { 
  document.getElementById('nearInput').value=val; 
  near= val;
}
function updateFar(val) { 
  document.getElementById('farInput').value=val;  
  far= val;

}
function updateEyex(val) {
  document.getElementById('eyexInput').value=val; 
  eye[0]=val;
  
 }
function updateEyey(val) { 
  document.getElementById('eyeyInput').value=val; 
  eye[1]=val;
  
}
function updateEyez(val) { 
  document.getElementById('eyezInput').value=val;
  eye[2]=val;
  
}
function updateAtx(val) { 
  document.getElementById('atxInput').value=val;  
  at[0]= val;
 
}
function updateAty(val) { 
  document.getElementById('atyInput').value=val; 
  at[1]= val;
  
}
function updateAtz(val) { 
  document.getElementById('atzInput').value=val; 
  at[2]= val; 
  
}
function updateUpx(val) { 
  document.getElementById('upxInput').value=val;
  up[0]= val; 
  
}
function updateUpy(val) { 
  document.getElementById('upyInput').value=val; 
  up[1]= val; 
  
}
function updateUpz(val) { 
  document.getElementById('upzInput').value=val; 
  up[2]= val; 
   
}